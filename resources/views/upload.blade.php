@extends('layouts.master')

@section('after_styles')
    <link href="{{ url('css/custom.css')}}" rel="stylesheet" type="text/css">
@endsection

@section('content')
<div class="dashboard-wraper">
    <div class="container mt-5">
        <form action="{{route('fileUpload')}}" method="post" enctype="multipart/form-data" class="theme-form">
          <h3 class="text-center mb-5">Upload File in Laravel</h3>
            @csrf
            @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <strong>{{ $message }}</strong>
            </div>
          @endif

          @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
          @endif

            <p class="alert alert-info mt-4">
                Only allow file are .jpg, .gif, .mp3,.mp4 
            </p>
            <div class="custom-file">
                <input type="file" name="file" class="custom-file-input" id="chooseFile">
                <label class="custom-file-label" for="chooseFile">Select file</label>
            </div>

            <select class="form-control mt-4" name="provider_id">
                <option disabled>Select Provider</option>
                <option value="1">Google</option>
                <option value="2">Snapchat</option>
            </select>

            <button type="submit" name="submit" class="btn btn-primary btn-block mt-4">
                Upload Files
            </button>
        </form>
    </div>
</div>
@endsection

@section('after_scripts')
<script src="{{ url('js/jquery-ui.min.js')}}" type="text/javascript" charset="utf-8"></script>
@endsection
