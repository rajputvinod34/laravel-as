@extends('layouts.api_master')
{{-- Page title --}}
@section('title')
Message
@parent
@stop
{{-- page level styles --}}
@section('header_styles')
@stop
{{-- Page content --}}
@section('inner_body')
<?php $response = $setting['default_response']; ?>
<div class="right_col" role="main" style="min-height: 3214px;">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>
                <span>{{$setting['method']}} </span>
                <small>{{route($setting['action'])}}</small>&nbsp;
                <button onclick="copyToClipboard(this)" class="btn btn-success btn-sm" id="copyBtn" data-copytext="{{route($setting['action'])}}">Copy Link</button>
                <span class="green" id="copiedText" style="font-size:16px;"></span>
                </h3>
            </div>
            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search"></div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-8 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>API Form</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <form class="form-horizontal form-label-left" method="{{$setting['method']}}" action="{{route($setting['action'])}}">
                            <select class="form-control mt-4" name="provider_id">
                                <option disabled>Select Provider</option>
                                <option value="1">Google</option>
                                <option value="2">Snapchat</option>
                            </select>
                            <br/>
                            <div class="alert alert-info">
                                <b>Google</b>
                                <p>
                                .jpg
                                Must be in aspect ratio 4:3 < 2 mb size
                                </p>
                                <p>
                                .mp4
                                < 1 minutes long
                                </p>
                                <p>
                                .mp3
                                < 30 seconds long< 5mb size
                                </p>
                            </div>
                            <div class="custom-file">
                                <input type="file" name="file" class="custom-file-input" id="chooseFile">
                                <label class="custom-file-label" for="chooseFile">Select file</label>
                            </div>

                            @if(!empty($form))
                            @foreach($form as $key => $val) 
                            

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12 {{$val['type']}}">{{$key}}<span class="required">{{$val['required']}}</span></label>
                                <div class="col-md-9 col-sm-9 col-xs-12">

                                    @if($val['type'] == 'checkbox')
                                        <input type="{{$val['type']}}" name="{{$key}}">
                                    @endif 

                                    @if($val['type'] == 'text')
                                        <input type="{{$val['type']}}" name="{{$key}}" value="{{$val['value']}}" class="form-control" placeholder="{{!empty($val['placeholder'])?$val['placeholder']:''}}">
                                    @endif 

                                    @if($val['type'] == 'file')
                                        <input multiple type="{{$val['type']}}" name="{{$key}}" value="{{$val['value']}}" class="form-control" placeholder="{{!empty($val['placeholder'])?$val['placeholder']:''}}">
                                    @endif 

                                    @if($val['type'] == 'password')
                                        <input type="{{$val['type']}}" name="{{$key}}" value="{{$val['value']}}" class="form-control" placeholder="{{!empty($val['placeholder'])?$val['placeholder']:''}}">
                                    @endif 

                                    @if($val['type'] == 'radio')
                                        @php($i = 0)
                                        @foreach($val['value'] as $objVal)
                                        <input type="{{$val['type']}}" name="{{$key}}" value="{{$objVal}}"> {{$val['value_text'][$i]}}
                                        @php($i++) 
                                        @endforeach                                     
                                    @endif                                    
                                </div>
                            </div>
                            @endforeach
                            @endif                            
                                                       
                            <div class="form-group">
                                <br/>
                                <button type="submit" id="frm_submit_btn" class="btn btn-info pull-right">Submit</button>
                            </div>                    
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                @include('layouts.response')
            </div>
        </div>
    </div>
</div>    
@stop
{{-- page level scripts --}}
@section('footer_scripts')
<script type="text/javascript">
   function copyToClipboard(obj) {
        console.log('copyToClipboard');
        var $temp = $("<input>");
        $("body").append($temp);
        $temp.val($(obj).data('copytext')).select();
        document.execCommand("copy");
        $temp.remove();
        $('#copiedText').text('Copied!');
        }
    $(document).ready(function(){
     
    });
</script>   
@stop
