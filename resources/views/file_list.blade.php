
@extends('layouts.master')

@section('after_styles')
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
    <link href="{{ url('jquery-confirm/css/jquery-confirm.min.css')}}" rel="stylesheet" type="text/css">
@endsection
@section('content')
<div class="dashboard-wraper container">
    <div class="content-wrap">
        <div class="dashboard-content box_shadow p-0">
            <div class="people_sec people_page_sec">
                <div class="row">
                    <div class="col-md-12">
                        <form class="overflow-auto">
                          <table class="table people_table" id="fileList" style="width: 100%;" cellspacing="0">
                            <thead class="thead-dark">
                                <tr>
                                    <th scope="col">ID</th>
                                    <th scope="col">NAME</th>
                                    <th scope="col">PROVIDER</th>
                                    <th scope="col">FILE TYPE</th>
                                    <th scope="col">Upload Date</th>
                                    <th scope="col">ACTION</th>
                                </tr>
                            </thead>
                            <tbody>
                                <!-- table rows -->
                            </tbody>
                          </table>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- /. End dashboard-content -->
@endsection

@section('after_scripts')
<script src="{{ asset('/js/jquery-3.3.1.min.js') }}"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<script src="{{ url('jquery-confirm/js/jquery-confirm.min.js')}}"></script>
<script>
    let dtOverrideGlobals = {
        responsive: true,
        "processing": true,
        "serverSide": true,
        "bVisible": false, 
        "searching": true, 
        "paging": true,
        "ajax": "file-list",
        columns: [
            {data: 'id', name: 'id'},
            {data: 'name', name: 'name', className:'color-r'},
            {data: 'provider_name', name: 'provider_name'},
            {data: 'file_type', name: 'file_type'},
            {data: 'created_at', name: 'created_at'},
            {data: 'action', name: 'action', className:'color-r', orderable: false, searchable: false},
        ],
        columnDefs: [
        { "width": "120px", "targets": [0] , 'orderable': false}]
    };
    var table = $('#fileList').DataTable(dtOverrideGlobals);


    //================================================ DELETE FILE ===============================================//
    $('#fileList').on('click', 'a.deleteFile', function(){
        let file_id = $(this).data('file_id');
        openDeleteFileConfirmationPopup(file_id);
    });

    function openDeleteFileConfirmationPopup(file_id) {
        $.confirm({
            title: 'Delete File?',
            content: 'Are you sure you want to delete this file??',
            type: 'red',
            typeAnimated: true,
            buttons: {
                tryAgain: {
                    text: 'YES',
                    btnClass: 'btn-green',
                    action: function(){
                        // update file
                        deleteFile(file_id);
                    }
                },
                NO: function () {
                }
            },
        });  
    }

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    function deleteFile(file_id) {
        $.ajax({
            data: {'file_id':file_id},
            url: "file/delete",
            type: "POST",
            dataType: 'json',
            success: function (data) {
                table.draw();
                return true;
            },
            error: function (data) {
                $(".progres_bar_ex").remove();
                alert("Failed please try again!!")
                return true;    
            }
        });
    }


    //=============================== File Preview ================================//

    $('#fileList').on('click', 'a.viewFile', function(){
        let file_id = $(this).data('file_id');
        openFilePreviewPopup(file_id);
    });

    function openFilePreviewPopup(file_id) {
        $.confirm({
            title: 'Preview File',
            content: '<span id="addloading">loading...<br></span> <div id="filePreview"></div>',
            type: 'green',
            typeAnimated: true,
            buttons: {
                close: function () {
                }
            },
            onOpenBefore: function () {
                $.get("file-details" +'?file_id=' + file_id , function (response) {
                    $("#addloading").hide();
                    if (response.type =='success') {
                        var data = response.data;
                        console.log(data);
                        if(data.file_type =="video") {
                            $("#filePreview").html('<video width="320" height="240" controls><source src="'+data.file_path+'" type="video/mp4"></video>')
                        } else if (data.file_type =="image") {
                            $("#filePreview").html('<img id="blah" src="'+data.file_path+'" alt="your image" />')
                        } else if (data.file_type =="audio") {
                            $("#filePreview").html('<audio controls><source src="'+data.file_path+'" type="audio/mpeg"></audio>')
                        } 
                    }
                });
            },
        }); 
    }

</script>
@endsection
