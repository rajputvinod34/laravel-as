<footer class="footer-sec">
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-4 footer-widget ftr-customers pl-xl-5 pl-md-4 pl-3">
                    <h4 class="footer-ttl">For Customers</h4>
                    <ul class="footer-menu">
                        <li><a href="#">About Laravel</a></li>
                        <li><a href="#">Support</a></li>
                        <li><a href="#">Terms & Conditions</a></li>
                    </ul>
                </div>
                <div class="col-lg-3 col-md-4 footer-widget ftr-links pl-xl-5 pl-md-4 pl-3">
                    <h4 class="footer-ttl">Quick Links</h4>
                    <ul class="footer-menu">
                        <li><a href="{{ url('about-us') }}">About Us</a></li>
                        <li><a href="{{ url('faq') }}">FAQ</a></li>
                        <li><a href="{{ url('contact-us') }}">Contact Us</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- ./ End footer Top -->
    <div class="footer-copyright">
        <div class="container">
            <p>Laravel ©{{ now()->year }} All Rights Reserved</p>
        </div>
    </div>
</footer>
@section('after_scripts')
<script type="text/javascript">   
    AOS.init({
        disable: function() {
            var maxWidth = 768;
            return window.innerWidth < maxWidth;
        }
    }); 
</script>
@endsection
</body>
</html>