<header class="header-sec">
    <nav class="navbar navbar-expand-lg navbar-header">
        <div class="container">
            <a class="navbar-brand logo-brand header-logo" href="{{ url('/') }}">
                <img src="{{ url('images/logo.png') }}" alt="LOGO">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse header-menu justify-content-md-end" id="navbarNavDropdown">
                <ul class="navbar-nav main-menu">
                    <li class="nav-item active">
                        <a class="nav-link" href="{{ url('/') }}">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('upload') }}">Upload Files</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('file-list') }}">View Uploaded Files</a>
                    </li>
                </ul>
            </div>
            <!-- ./ End menu -->
            <!-- @if(auth()->user())
            <div class="user-profile dropdown">
                <a class="nav-link dropdown-toggle text-white" href="#" id="userMenuprofile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="fa fa-user"></span> {{ auth()->user()->first_name }} {{ auth()->user()->last_name }}
                </a>
                <ul class="user-prof-menu dropdown-menu">
                    <li><a href="{{ url('file_list') }}"><i class="fa fa-tachometer" aria-hidden="true"></i>File List</a></li>
                </ul>
            </div>
            @else
            <div class="login-reg">
                <a href="{{ url('login') }}" class="btn-login btn btn-theme btn-green btn-small mr-sm-2">Login</a>
                <a href="{{ url('register') }}" class="btn-reg btn btn-theme btn-small">Sign Up</a>
            </div>
            @endif -->
            <!-- ./ End login-reg -->
        </div>
        <!-- ./ End Container -->
    </nav>
</header>