<!DOCTYPE html>
<html lang="{{ (config('app.locale')) }}"{!! (config('lang.direction')=='rtl') ? ' dir="rtl"' : '' !!}>
<head>
	<meta charset="utf-8">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<meta name="viewport" content="width=device-width,initial-scale=1.0, user-scalable=no"/>
	<meta name="apple-mobile-web-app-title" content="{{ config('settings.app.app_name') }}">
	<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
	<title>{{ (config('app.name')) }}</title>
	
    @yield('before_styles')
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
	<link href="{{ url('css/global.css')}}" rel="stylesheet" type="text/css">
	@yield('after_styles')
	
	<script>
		paceOptions = {
			elements: true
		};
	</script>
</head>
<body class="{{ config('app.skin') }} {{ isset($bodyClass) ? $bodyClass : ''}}">
<div class="overlay"></div>

@section('header')
	@include('layouts.inc.header')
@show

@yield('content')

@section('info')
@show

@section('footer')
	@include('layouts.inc.footer')
@show

<script>
	{{-- Init. Root Vars --}}
	var siteUrl = '<?php echo url((config('app.locale') ? config('app.locale') : '' ) . '/'); ?>';
</script>

@yield('before_scripts')

@yield('after_scripts')
</body>
</html>
