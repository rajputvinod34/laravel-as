<div class="menu_section">
    <h3>API's</h3>
    <ul class="nav side-menu">
        <li>
            <a>File Upload<span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
                <li><a href="{{route('view_api','file-upload')}}">Upload File</a></li>
                <li><a href="{{route('view_api','file-list')}}">Get Uploaded File List</a></li>
            </ul>
        </li>
    </ul>
</div>