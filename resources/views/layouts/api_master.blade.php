<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Laravel Api panel</title>
    <link rel="stylesheet" type="text/css" href="{{asset('css/admin-theme.min.css')}}">
    <style type="text/css">
        .horizontal-center {
            width: 48%;
            margin: 11px auto;
        }
    </style>
    @yield('header_styles')
</head>

<body class="nav-md">
    <div class="container body">
        <div class="main_container">
            <div class="col-md-3 left_col">
                <div class="left_col scroll-view">
                    <div class="navbar nav_title" style="border: 0;">
                        <a href="#" class="site_title" style="color:#22dd30;font-weight:bold"><span style="color:#000000;font-weight:bold"></span><span style="color:#22dd30;font-weight:bold">API Panel</span></a>
                    </div>
                    <div class="clearfix"></div>
                    <!-- menu profile quick info -->
                    @if(!empty($menu_type))
                    <?php
                    $user_rec = Auth::user();
                    $image = $user_rec->user_image;
                    $image = !empty($image) ? $image : "";
                    if (!file_exists($image)) {
                        $image = "uploads/nophoto.png";
                    }
                    $image = url('/') . '/' . $image;
                    $name = $user_rec->name;
                    ?>
                    <div class="profile clearfix">
                        <div class="profile_pic">
                            <img src="{{$image}}" alt="..." class="img-circle profile_img">
                        </div>
                        <div class="profile_info">
                            <span>Welcome,</span>
                            <h2>{{$name}}</h2>
                        </div>
                    </div>
                    @endif
                    <!-- /menu profile quick info -->
                    <br />
                    <!-- sidebar menu -->
                    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                        @include('layouts.menu')
                    </div>
                </div>
            </div>
            <!-- top navigation -->
            <div class="top_nav">
                <div class="nav_menu">
                    <nav>
                        <div class="nav toggle">
                            <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                        </div>
                        @if(!empty($menu_type))
                        <ul class="nav navbar-nav navbar-right">
                            <li class="">
                                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">

                                    <img src="{{$image}}" alt=""> {{ $name }}
                                    <span class=" fa fa-angle-down"></span>
                                </a>
                                <ul class="dropdown-menu dropdown-usermenu pull-right">
                                    <li><a href="{{route('get_admin_profile')}}"> Profile</a></li>
                                    <!--  <li>
                                            <a href="javascript:;">
                                                <span class="badge bg-red pull-right">50%</span>
                                                <span>Settings</span>
                                            </a>
                                        </li>
                                        <li><a href="javascript:;">Help</a></li> -->
                                    <li><a href="{{route('admin_logout')}}"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                                </ul>
                            </li>


                        </ul>
                        @endif

                    </nav>
                </div>
            </div>
            <!-- /top navigation -->
            <!-- page content -->
            @yield('inner_body')
            <!-- /page content -->
            <!-- footer content -->
            <footer>
                <div class="pull-right">
                    Laravel
                </div>
                <div class="clearfix"></div>
            </footer>
            <!-- /footer content -->
        </div>
    </div>

    <script src="{{asset('js/admin-theme.min.js')}}"></script>

    @if(empty($menu_type))
    <script type="text/javascript">
        $(document).ready(function() {
            var navcur = window.location.href;
            $("a[href='" + navcur + "']").parent('li').css('background', 'currentColor').parents('ul').css('display', 'block');

            $("form").on('submit', function(e) {
                e.preventDefault();
                var msg = "Loading...";
                $(".api_response textarea").val(msg);
                var formData = new FormData(this);
                let auth_token = '';
                auth_token = (formData.get('auth_token')) ? formData.get('auth_token') : '';
                $.ajax({
                    url: $('form').attr('action'),
                    headers: {
                        'Authorization': 'Bearer ' + auth_token,
                        //remove content type json while sending webform
                    },
                    type: $('form').attr('method'),
                    contentType: false,
                    processData: false,
                    data: formData,
                    success: function(res) {
                        console.log('success res', res);
                        // var pretty = JSON.stringify(obj, undefined, 4);
                        setTimeout(function() {
                            $(".api_response textarea").val(JSON.stringify(res, undefined, 4));
                        }, 1000);
                    },
                    error: function(res) {
                        console.log('error res', res.responseText);
                        setTimeout(function() {
                            $(".api_response textarea").val(JSON.stringify(res, undefined, 2));
                        }, 1000);
                    }
                });
            });
            var ugly = $(".api_response textarea").val();
            var obj = JSON.parse(ugly);
            var pretty = JSON.stringify(obj, undefined, 4);
            $(".api_response textarea").val(pretty);
        });
    </script>
    @else
    <script type="text/javascript">
        $(document).ready(function() {
            var currentClass = '{{!empty($current_class)?$current_class:""}}';
            if (currentClass != '') {
                $('.' + currentClass).parent('li').addClass('current-page').parent('ul');
                $('.' + currentClass).parent('li').parent('ul').show().parent('li').addClass('active');
            }
        });
    </script>
    @endif
    @yield('footer_scripts')
</body>

</html>