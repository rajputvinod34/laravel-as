<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/home', function () {
    return view('welcome');
});

Route::group([
	'prefix'     => ''
], function ($router) {
	Route::group(['middleware' => ['web']], function ($router) {
		Route::get('/', 'FileUploadController@createForm');
		Route::get('/upload', 'FileUploadController@createForm');
		Route::post('/upload', 'FileUploadController@fileUpload')->name('fileUpload');
		Route::get('/file-list', 'FileUploadController@displayFileList');

		// Delete file
		Route::post('/file/delete', 'FileUploadController@fileDelete')->name('fileDelete');

		// Get File details 
		Route::get('file-details', 'FileUploadController@getFileDetails');
	});
});