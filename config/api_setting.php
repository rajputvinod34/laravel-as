<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Application Name
    |--------------------------------------------------------------------------
    |
    | This value is the name of your application. This value is used when the
    | framework needs to place the application's name in a notification or
    | any other location as required by the application or its packages.
    */


    'file-upload' => [
        'action' => 'file-upload',
        'method' => 'POST',
        'default_response' => '{"success": true,"message": "File has been successfully uploaded."}'
    ],
];
