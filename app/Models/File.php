<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon;

class File extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'file_path'
    ];


    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */
    public function getCreatedAtAttribute($value)
    {
    	$timezone = env('APP_TIMEZONE','UTC');
        $dt = Carbon::parse($value)->timezone($timezone);
        return $dt->format(config('app.date_format','m-
        	d-Y'));
    }
}
