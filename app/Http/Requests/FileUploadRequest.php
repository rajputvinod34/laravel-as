<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FileUploadRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];
        $providerId = request()->provider_id;

        if($providerId==1) {
            $rules = [
                        'file' => 'required',
                        'file' => 'mimes:jpg,mp4,mp3'
                    ];
            if ($this->hasFile('file')) {
                $file = $this->file('file'); 

                $fileType = $file->getClientOriginalExtension();
                if ($fileType =='jpg') {
                    $rules = [
                            'file' => 'size:2048|dimensions:ratio=4/3'
                        ];
                }

                if ($fileType=='mp3') {
                    $rules = [
                            'file' => 'max:5120'
                        ];
                }
            }
        }

        if($providerId == 2) {
            $rules = [
                        'file' => 'required',
                        'file' => 'mimes:jpg,gif,mp4,mov'
                    ];
            if ($this->hasFile('file')) {
                $file = $this->file('file'); 

                $fileType = $file->getClientOriginalExtension();
                if ($fileType=='jpg' || $fileType=='gif') {
                    $rules = [
                            'file' => 'max:5120|dimensions:ratio=16/9',
                        ];
                }

                if ($fileType=='mp4' || $fileType=='mov') {
                    $rules = [
                            'file' => 'max:51200'
                        ];
                }
            }
        }

        //print_r($rules);die;
        return $rules;
    }

    public function messages()
    {
        $messages = [];
        $providerId = request()->provider_id;

        if($providerId == 1) {
            $messages = [
                        'file.required' => "You must use the 'Choose file' button to select which file you wish to upload",
                        'file.mimes' => "Allow only JPG, MP4, MP3 files.",
                    ];
            if ($this->hasFile('file')) {
                $file = $this->file('file'); 

                $fileType = $file->getClientOriginalExtension();
                if ($fileType=='jpg') {
                    $messages = [
                            'file.max' => "Maximum file size to upload is 2MB (2048 KB). If you are uploading a photo, try to reduce its resolution to make it under 2MB",
                            'file.dimensions' => "Uploaded image ratio should be 4:3 ratio",
                        ];
                }

                if ($fileType=='mp3') {
                    $messages = [
                            'file.max' => "Maximum file size to upload is 5MB (5120 KB).",
                        ];
                }
            }
        }

        if($providerId==2) {
            $messages = [
                        'file.required' => "You must use the 'Choose file' button to select which file you wish to upload",
                        'file.mimes' => "Allow only JPG, GIF, MP4, MOV files.",
                    ];
            if ($this->hasFile('file')) {
                $file = $this->file('file'); 

                $fileType = $file->getClientOriginalExtension();
                if ($fileType=='jpg' || $fileType=='gif') {
                    $messages = [
                            'file.max' => "Maximum file size to upload is 5MB (5120 KB). If you are uploading a photo, try to reduce its resolution to make it under 5MB",
                            'file.dimensions' => "Uploaded image ratio should be 16:9 ratio",
                        ];
                }

                if ($fileType=='mp4' || $fileType=='mov') {
                    $messages = [
                            'file.max' => "Maximum file size to upload is 50MB (51200 KB).",
                        ];
                }
            }
        }

        // print_r($messages);
        return $messages;
    }
}
