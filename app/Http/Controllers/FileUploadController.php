<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\File;
use Illuminate\Support\Facades\DB;
use DataTables;

class FileUploadController extends Controller
{
    public function createForm(){
	    return view('upload');
  	}

  	public function fileUpload(Request $req){
        $req->validate([
        'file' => 'required|mimes:jpg,gif,mp3,mp4|max:2048'
        ]);

        $fileModel = new File;

        if($req->file()) {

            $fileType = $req->file->getClientOriginalExtension();

            if (in_array($fileType, array('mp4','mov'))) {
                $fileType = 'video';
            } 

            if (in_array($fileType, array('mp3'))) {
                $fileType = 'audio';
            }

            if (in_array($fileType, array('jpg','gif'))) {
                $fileType = 'image';
            }

            $fileName = time().'_'.$req->file->getClientOriginalName();
            $filePath = $req->file('file')->storeAs('uploads', $fileName, 'public');

            $fileModel->name = time().'_'.$req->file->getClientOriginalName();
            $fileModel->file_path = '/storage/' . $filePath;
            $fileModel->file_type = $fileType;
            $fileModel->provider_id = $req->provider_id;
            $fileModel->save();

            return back()
            ->with('success','File has been uploaded.')
            ->with('file', $fileName);
        }
   	}

    public function displayFileList(Request $request) {
        $user = auth()->user();
        if ($request->ajax()) {
            $data = DB::table('files as f')
                    ->leftJoin('providers as p', function ($join) {
                        $join->on('f.provider_id', '=', 'p.id');
                    })
                    ->select('f.*', 'p.provider_name')
                    ->orderBy('id', 'desc')
                    ->get();
        return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                        $btn = ' <a href="javascript:void(0)" data-toggle="tooltip"  data-file_id="'.$row->id.'" data-original-title="Delete" class="btn btn-danger btn-sm deleteFile" title="Delete"><i class="fa fa-trash"></i></a>';
                        $btn = $btn.' <a href="javascript:void(0)" data-toggle="tooltip"  data-file_id="'.$row->id.'" data-original-title="Delete" class="btn btn-info btn-sm viewFile" title="View File"><i class="fa fa-eye"></i></a>';

                        return $btn.' <i id="loader'.$row->id.'" class="fas fa-sync fa-spin" style="display:none;"></i>';
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
        return View('file_list');
    }

    function fileDelete(Request $request) {
        $fileId = $request->input('file_id');
        $file = File::find($fileId);
        if ($file) {
            // unlink($file->file_path);
            $file->delete();
            return response()->json([
                'message'=>'Successfully deleted!',
                'type'=>'success'
            ]);
        } else {
            return response()->json([
                'message'=>'Failed!!',
                'type'=>'error'
            ]);
        }
    }

    public function getFileDetails(Request $request) {
        $fileId = $request->input('file_id');
        $file = File::find($fileId);
        if ($file) {
            return response()->json([
                'data' => $file,
                'type'=>'success'
            ]);
        } else {
            return response()->json([
                'type'=>'error'
            ]);
        }
    }
}
