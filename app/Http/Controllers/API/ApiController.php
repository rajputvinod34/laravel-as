<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use View;

class ApiController extends Controller
{
    public function index($page) {
        $api_setting = config("api_setting.$page");
        $form_setting = config("api_form.$page");
        if(isset(config("api_setting")[$page])){
            return View::make("api_form", array('form'=>$form_setting,'setting'=>$api_setting));
        }else{
            $data = array();
            $data['type'] = 'error';
            $data['message'] = "Invaid api url.";
            echo json_encode($data);
        }
            
    }
}
