$(document).ready(function (){

    jQuery('.client-slider').owlCarousel({
        loop:true,
        margin:0,
        nav:false,
        dots: false,
        autoplayTimeout:3500,
        autoplay:true,
        lazyLoad:true,
        smartSpeed: 1000,
        items:1
    });
   
});