<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Provider;

class ProviderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Provider::create(
        	[
				'provider_name' => 'Google'
			],
		);

		Provider::create(
        	[
				'provider_name' => 'Snapchat'
			],
		);
    }
}
